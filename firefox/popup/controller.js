// Request functions
function httpRequestAsync(httpRequest, header, url, params, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.setRequestHeader('Content-type', header);
  xmlHttp.onreadystatechange = function() {
    if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      if(callback != null)
        callback(xmlHttp.responseText);
    }
  }
  xmlHttp.open(httpRequest, url, true); // true for asynchronous 
  xmlHttp.send(params);
}

function httpGetAsync(url, callback)
{
  httpRequestAsync('GET', '', url, null, callback);
}

function httpPutAsync(url, params, callback)
{
  httpRequestAsync('PUT', 'application/json', url, params, callback);
}

function httpPostAsync(url, params, callback)
{
  httpRequestAsync('POST', 'application/json', url, params, callback);
}

function httpDeleteAsync(url, callback)
{
  httpRequestAsync('DELETE', '', url, null, callback);
}

function selectView(view) {
  let selectedView = document.getElementById(view);
  let activeContainerList = document.getElementsByClassName('container-active');
  for(var i=0; i<activeContainerList.length; i++) {
    activeContainerList[i].className = 'container';
  }
  selectedView.className = 'container-active';
}

// GUI functions
function viewRouter(view) {
  switch (view) {
    case 'add_view':
      // Display the main view
      selectView(view);
      break;
    case 'edit_view_s1':
      // Display the main view
      selectView(view);
      break;
    case 'edit_view_s2':
      var editViewTS = document.getElementById('et_selected_task');
      var callback = function(result) {
        //TODO
        var title = result[i].title;
        var start = result[i].dateBegin;
        var end = result[i].dateEnd;
        var status = result[i].status.id;
        var tags = result[i].tags;
        var titleField = document.getElementById('et_title');
        var startField = document.getElementById('et_start');
        var endField = document.getElementById('et_end');
        var statusField = document.getElementById('et_status');
        var tagsField = document.getElementById('et_tags');
        titleField.value = title;
        startField.value = start;
        endField.value = end;
        statusField.value = status;
        tagsField.value = tags;
      }
      httpGetAsync('http://localhost:8080/task/'+editViewTS.value, callback);
      // Display the main view
      selectView(view);
      break;
    default:
      // main_view
      // Reset values of various form
      let addViewForm = document.getElementById('add_view_form');
      let editViewS1Form = document.getElementById('edit_view_s1_form');
      let editViewS2Form = document.getElementById('edit_view_s2_form');
      addViewForm.reset();
      editViewS1Form.reset();
      editViewS2Form.reset();
      initToDoList();
      // Display the main view
      selectView(view);
      break;
  }
}

function addPlusBtn() {
  // button
  var addTaskNode = document.createElement('button');
  addTaskNode.id = 'add_btn';
  addTaskNode.innerHTML = '<i class="fas fa-plus"></i>';
  addTaskNode.onclick = 'initToDoList()';
  // listeners
  addTaskNode.onclick = function() {
    console.log('You click on \'add\' button');
    alert('You click on \'add\' button');
    viewRouter('add_view');
  };
  // assembling
  var tasks = document.getElementById('task-container');
  tasks.appendChild(addTaskNode);
}

function newTask(id, title, start, end, status, tags) {
  if(title != undefined && start != undefined && end != undefined && status != undefined && tags != undefined) {
    // container
    var taskNode = document.createElement('div');
    taskNode.className = 'task';
    // content
    var contentNode = document.createElement('div');
    contentNode.className = 'task-content';
    var titleNode = document.createElement('h2');
    titleNode.innerHTML = title;
    var durationNode = document.createElement('span');
    durationNode.className = 'duration';
    durationNode.innerHTML = '('+start+' - '+end+')';
    var statusNode = document.createElement('div');
    statusNode.className = 'status';
    statusNode.innerHTML = status;
    var tagsNode = document.createElement('div');
    tagsNode.className = 'tags';
    tagsNode.innerHTML = tags;
    var splitNode = document.createElement('hr');
    // actions
    var actionNode = document.createElement('div');
    actionNode.className = 'task-action';
    var editNode = document.createElement('button');
    editNode.className = 'action';
    editNode.innerHTML = '<i class="fas fa-edit"></i>';
    var delNode = document.createElement('button');
    delNode.className = 'action';
    delNode.innerHTML = '<i class="fas fa-trash-alt"></i>';
    // listeners
    editNode.onclick = function() {
      console.log('You click on \'edit\' button of task '+id);
      alert('You click on \'edit\' button of task '+id);
      viewRouter('edit_view_s1');
    };
    delNode.onclick = function() {
      console.log('You click on \'trash\' button');
      var callback = function() {
        console.log('The task has been deleted');
        alert('The task has been deleted');
      }
      httpDeleteAsync('http://localhost:8080/task/'+id, callback);
    };
    // assembling
    contentNode.appendChild(titleNode);
    contentNode.appendChild(statusNode);
    contentNode.appendChild(durationNode);
    contentNode.appendChild(splitNode);
    contentNode.appendChild(tags);
    actionNode.appendChild(editNode);
    actionNode.appendChild(delNode);
    taskNode.appendChild(contentNode);
    taskNode.appendChild(actionNode);
    var tasks = document.getElementById('task-container');
    tasks.appendChild(taskNode);
  }
}

function initToDoList() {
  // Request 
  var callback = function(result) {
    // Display
    var editViewTS = document.getElementById('et_selected_task');
    var tasks = document.getElementById('task-container');
    editViewTS.innerHTML = '';
    tasks.innerHTML = '';
    for(var i=0; i<result.length; i++) {
      var newOption = document.createElement('option');
      var optionLabel = (result[i].tags != '') ? result[i].title+' (#'+result[i].tags.replace(',', ' #')+')' : result[i].title;
      newOption.value = result[i].id;
      newOption.innerHTML = optionLabel;
      editViewTS.appendChild(newOption);
      newTasknewTask(result[i].title, result[i].dateBegin, result[i].dateEnd, result[i].status.id, result[i].tags);
    }
    addPlusBtn();
  };
  httpGetAsync('http://localhost:8080/tasks', callback);
}
